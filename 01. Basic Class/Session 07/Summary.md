adrian/ 28 April 2020

# What i've learned

## READLINE
Readline is a `module` form javascript for read our codes line by line
```js
const readline = require("readline");//the last readline means the readline modul we summon
```
and usually followed by createInterface function to create Readline interface
```js
const rl = readline.creatInterface (
    process.stdin,
    process.stdout
    );
```
function how to get an answer from user and process it to the result with readline
 ```js
 function getAreaInput() {
    rl.question("Side: ", answer => {//answer is a parameter 
        console.log("Here is the result", square.area(+answer)// +(plus) is for converting string answer to number
        )
        rl.close()// to close the readline after answering the question
    })
}
```
handle answer with if function
```js
function handleAnswer(answer) {
    if (answer == 1) getAreaInput();
    if (answer == 2) getRoundInput();
}

rl.question("Answer: ", answer => {
    handleAnswer(answer);
})
```
## SwitchCase
```js
let answer = 1
switch(answer) {
    case 1:
        break; console.log(<variable>)
    case 2: 
        break; console.log(Variable)
    default:
    console.log("message for option not available)
}
```
atau
```js
let answer = 1
switch(answer) {
    case 1: return <variable>
    case 2: Return <variable>
    default:
    console.log("message for option not available)
}
```
## Import and Export modul
first we have to make our own modul like kotak.js(whicn=h is filled with only function/formula to do someting) and filed it with funtion:
```js
module.exports = {
    <function variable>
    };
    ```
then we call it in the main index java script with
```js
const <variable> = require('whereyourfileandwhatitisname');
}
```
## Regular Expression 
or usually called `Regex`is an operation to validating that the data is strict to not contain something
```js
let numberOnly = /^[0-9]+$/ //the $ symbol is for stricting the data
//how to check
numberOnly.test("12345")
// the answer is True or false