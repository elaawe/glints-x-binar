/*
1. create a class called human
2. create sub class called chef

human can cook();
human can introduce:

chef cuissines;
chef type => italian, french;
chef can promote():
chef can cook() but better // method overide
chef will introduce themself as ${type} chef
*/

Object.defineProperty(Array.prototype, 'sample', {
    get: function() {
        return this[
            Math.floor(
                Math.random() * this.length
            )
        ]
    }
});



class Human {
    constructor(props) {
        let { name, address, job} = props;
        this.name = props.name;
        this.address = props.address;
        this.job = props.job;
    }

    introduce() {
        console.log(`Hello My Name is ${this.name} I am a ${this.job}`);
    }
    cook() {
        console.log("I can cook Fried rice")
    }

}

class Chef extends Human {

    constructor(props) {
        super(props);
        
        this.type = props.type;
        this.cuissine = props.cuissine.sample;
    }

    promote() {
        console.log(`Come to ${this.address}, I will cook you a delicious ${this.cuissine}`);
    }

    cook() {
        super.cook() ;
        console.log(`and also I can cook ${this.cuissine} better!`);
    }
}


const adrian = new Chef({
    name: "Adrian",
    address: "jogja",
    job: "Chef",
    type: "Javanese",
    cuissine: ["Gudeg", "Rawon"]

})
adrian.introduce();
adrian.promote();
adrian.cook();