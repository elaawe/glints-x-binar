class Human {
    constructor(props) {
        let {name, lang, isMarried} = props;
        this.name = name;
        this.lang = lang;
        this.isMarried = isMarried
       
    }
    //Introducing from first Human
    introduce(nextHuman) {
        console.log(`Hi! My Name is ${this.name}, nice to meet you ${nextHuman.name}! `)
    }

    //the first human informing their marital status
    maritalStatus() {
        console.log(this.isMarried == true ? "I am married" : "I\'m single")
    }
    // this method will matching between first human and next human object 
    matchingHuman(nextHuman) {
        if(nextHuman.lang == this.lang) {// to make sure they have same language to communicate 
             console.log('I feel matched with you!');{ 
                if(nextHuman.isMarried == true) {// to check whether next human marital status is available or not
                    console.log('I think we should mind own business'); 
                } else {
                    console.log('Lets Get Married!');// if it comes to false or her marital status are avaible so they will get married!
                }            
            }
        } else {
            console.log('I think I should learn your Language ASAP!');// first human should learn the next human language
        }
    }  
}

const Adrian = new Human({
    name: 'Adrian',
    lang: 'Jawa',
    isMaried: false
})
const Pevita = new Human({
    name: 'Gege Elisa',
    lang: 'Jawa',
    isMaried: false
})

Adrian.introduce(Pevita);
Adrian.maritalStatus();
Adrian.matchingHuman(Pevita);