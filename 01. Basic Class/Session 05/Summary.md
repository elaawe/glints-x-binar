adrian/24 April 2020
# JAVA FUNDAMENTAL

## what statement is?
statement is an information/an orders that given in the Text editor, statement usualy end with this symbol (;)

statement as an information:
```bash
let A = 10;
```
statement as an Order:
```bash
console.log("hello world");
```

## Declaration
Declaration is the statement when you want to express a new variable. There is 3 command to declare a statement:
```javascript
let <variable name> = <value>; 
//usually used when whe want to only declare avariable once and only reassign to change the value
```
```javascript
var <variable name> = <value>;
//usually used when you wan to declare a variable more than one time, it takes a lot of memory than "let' because you have to declare many times.
```
```javascript
const <variable name> = <value>;
// usually used when you want to declare a new constan variable that means you cant change it or in other word its immuteable.
```
## Function
function is a statement to make a formula to calculate something in javascript
```javascript
function squareArea(x, y){
    console.log(x * y);
}
squareArea(10, 5);
```
```javascript
const squareArea(b, c){
    console.log(b * c)
}
squareArea(3, 5);
```
## Comment Command
we can put a comment inside of the text editor that not affect the other command/statement/function
```javascript
we can put "//"(comment in line) to comment in a line
```
```javascript
we can put
/*
<fill your comment here>
*/
for multi comment
```
## Return
return is the command for returning the value to the computer memory so it can be use for the next formula that related to the previous formula value.

Return should followed with an expression
```javascript
function rectangleArea(l, w){
    return l * w;
}

function cubeVolume(l, w, h){
    return rectangleArea * h;
}
console.log(cubeVolume(10, 10, 10))
```
## Arrow function
arrow function is the independet function, thats mean there is no any other properties except function itself.
```javascript
const calcRectangleArea = (l, w) => {
    return l * w;
}
//but you cant simplicity that function to be like this(only in arrow function)
const calcRectangleArea = (l, w) => l *w;
```
## Function Naming Rule
there's a rule for naming a functing

for `let` and `var` use camelCase rule, we should write uppercase in the first next word alphabet
```javascript
let squareArea(x,y);
```
for `const` we shou =ld use Uppercase for every word and separated by underscore (_)it usually called SNAKE_CASE
```javascript
const BLUE_COLOR = #fff000;
```
## DATA TYPE
1. Number
    1. Integer (1-1000)
    2. Big Integer(more than 1000)
    3. float (decimals with not much number after coma)
    4. Double(can handle much more number after coma than float)
2. String (a word data type)
3. Bolean (true false)
4. null (empty value)
5. Undefined (cant define the value)
6. object
7. Array
8. set
9. symbol