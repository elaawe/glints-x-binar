adrian/27 April 2020
# Java Fundamental
Today learned a bit deeper about java fundamental.

## Primitive Data
is the basic data that very language use, will directly save to the memory (number,String, bolean)

## Method
Every data have their own behavior, for example string:
```javascript
method.lenght //this is use fore measure how many characte is the string
```
## how do we know what data is the function or variable or value?
yaitu menggunakan `typeof` command, typeof have 2 function, such as a operator & as aFunction itself
```javascript
console.log(typeof <nameofvariable>);
```
## If else Statement
is a function that makes a statement will produce a value according to some estimated situation

if else statement will only read a `bolean` data
```java script
let raining = true;//variable Global
let midnight = true;//variable Global

if (raining && !midnight) {
  console.log("Pake payung");
} else if (raining && midnight) {
  console.log("Pesen Gocar");
} else {
  console.log("Gak usah pake payung");
};
//according to the variable, this function will produce "pesen gocar" value.
```
### Validation
is a function to validate what data type we want to our function using if else statement

    ```javascript
    function register(email, password) {
    if (typeof email === 'string' && typeof password === 'string') {
    console.log('Registering....');
      } else {
        throw new Error("Input is not valid!");
      };
    }
    register('string1', 'string2');
    ```
    ```javascript
    function register(email, password) {
    if (typeof email !== 'string' && typeof password !== 'string') {
        throw new Error("Input is not valid!");
        };

    console.log('Registering....');
    }
    ```
## Loop Function
there's 2 kind of loop: `for loop` and `while loop`. loop is using array data so there will be an index when you want to call the loop fucntion
```javascript:
 //For Loop
(let i = 0; i < 100; i++) {
    console.log("Hello World");
//'i' is the varible of index you can name it as you wish.

//While Loop
let b = 100;
while (b > 0) {
    console.log('Hello World lagi');
    b--;
// Developers usually use for loop bcs its more simple
```
## Array Data
is a list of a data that sorted by a index who started from zero(0), and using [] for the variable value

```javascript
let arr = [
  "Apple",
  "Manggo",
  "Durian",
  "Orange",
  "Banana"
]

for (let i = 0; i < arr.length; i++) {
  if (arr[i] === "Durian") {
    console.log(arr[i] + ' is a stinky fruit')
  } else {
    // Apple is a delicious fruit
    console.log(arr[i] + ' is a delicious fruit');
  }
}
```
how to get the last value of array index ?
```js
console.log(arr[arr.length - 1]);
```
how to add a new variable to existing index?
```js
arr.push("Tomato");// to add new variable on the end of index
console.log(arr)// just incase you want to show it again so we use console.log
```
another array function
```js
arr.pop();//remove the last element
arr.shift();//remove the first element
arr.unshift();// to add new variable value to the first index
arr.slice();// to add new variable value to the middle of index
```
## logical operator
`&&` equal to the true value if both of the variable value is true

`||` equal to the false value if one of the variable value is false no matter if the other else true

## math function
to round out the decimals value
```js
console.log(Math.floor(1.23123123)); // will round out down
console.log(Math.ceil(1.1231));// will round out up
```

## Object
object is a list of value that we can give a name for each value, we usually called it `key value`
```js
let numbers = [1,2,3,4,5]
console.log(numbers.join(","));

let obj = {
  name: "Fikri Rahmat Nurhidayat",
  isMarried: false,
  address: {
    city: "Solo",
    province: "Jawa Tengah"
  }
}

console.log(obj);
console.log(obj.address.city);
```
How to get the keys? incase we forgot it
```js
for (let i in obj) {
  console.log('Keys:', i);// this is how we search the 'keys'
  console.log(obj[i]);
}
```
### Nested Loop
is the loop inside of object function that will find the second layer of the obj function. because of this layer so we called it nested.
```js
  if (i == "address") {
    for (let b in obj[i]) {
      console.log(obj[i][b])// 'i' and 'b' can be replace with another character, and java will read it as first variable(i) and next variable(b)
    }
  }

```