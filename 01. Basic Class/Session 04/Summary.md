adrian/23 April 2020

# What i've learned Today?
Today we are reviewing previous day material adn do our firts Collaboration Project


### The Project
we are did some mini project using javascript, we did some merge job and see how it worked as if we do a real project with other developers. the first thing we do is:
1. create new Project using Gitlab
2. Invite Participant as a `Maintainer`(included our mentor)
3. and listing our java project (`Square.js,Circle. js, Triangle.js,Cube.js`)
4. make your own branch remote repository
    ```javascript
    git branch <yourbranchname>
    ```
5. each participant do one java project from the previous list. for example we will do the triangle project
6. How to make our java project:
    ```javascript
    function triangleArea(h,b){
        console.log(0.5*h*b);//this is the :Triangle Area Formula
    }
    triangleArea(5,8);//random variable
    ```
7. How do we see the result?
    ```javascript
    node Triangle.js
    ```
8. save it, add to git and commit to git and push it to your branch
    ```javascript
    git add Triangle.js
    ```

    ```javascript
    git commit -m "[adrian]Create Triangle.js"
    ```

    ```javascript
    git push origin <yourbranchname>
    ```
9. before we request to merge we need to see is there any commid changes or not from another developer, if there's then You need to syncronizing to your branch by pulling resources from master branch remote repository so their changes will not stacked down or error.
    ```javascript
    git pull origin master
    ```

    and then push it again 
    ```javascript
    git push origin <yourbranchname>
    ```
10. request merge and then wait until the verificator check all the code.