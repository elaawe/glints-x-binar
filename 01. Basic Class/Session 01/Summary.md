Adrian / 20 April 2020
# Basic class #


## What I've learned today? ##
Today I learn about basic class of what Back-end engineer is , what Terminal is, and what Node.js is

### Back End engineer ### 
- a man who cook the data, ensure the quality of data and validate the data that user want
- Back end later can pursue their career into a DevOps(Server and networking role)
- Back end need to know about :
    - Internet
    - Operating sistem (Debian series like Mint, ubuntu, etc) -->Terminal
    - Learn a language ( Programer must learn to switchable language depending on what world want) 
    - Version control (Forgot about this)
    - Learn database management system
    - learn about web API(who will trasfer all data that user need from back end to front end)
- Back end usually work as a team depend on the company authorities and how big is the company.
- Software house: is the third-party industriesy/vendor who provide for making and maintenance apps

### Terminal ###
- Shell is language to communicate with the Machine/hardware/computer it self
- Shell is used in Kernel, and kernel is a platform for Software and the machine to communicate
- in Ubuntu the kernel is known as Terminal
- shortcut for Ubuntu 18.04LTS Terminal is Ctrl + Alt + T
- Process is known as command word
- Argument is a detailed command goals
- Learned Process:
    - pwd = how to know which directory are we now
    - ls = how to know what inside the directory
    - cd = how to change the directory
    - cd . = how to change the directory
    - cd .. = how to go back to the parent directory
    - ~ = Known as Tilda sign, to represent home directory
    - mkadir = How to create a new folder/directory
    - ' '  = a mandatory sign for naming a new folder/file with space on the text name
    - touch = how to make a new file
    - cat = how to see the file content
    - mv = for rename and moving a file
    - cp = how to copy a file(you can also change the new copy file name)
    -
    
### Node.js ###
there is 2 kind of connector for javascript language
    - Interpreter/Translator and compiler
- Interpreter/translator is the one who will tranlate any words in realtime without compile it first to runtime environtment
- Compiler is the one who will translate any compiled script and send it to runtime environtment
- Node.js is interpreter so it will be more faster to interpretate language to runtime without compiling it first
- Node.js is the javascript language application made by Ryan Dahl so we didnt have to open some browser to translate javascript language
- to edit or write the javascript language we need a text editor like VS CODE

## Today I made my first Javascript ##
