const router = require('express').Router();
const productController = require('./controller/productController');
const userController = require('./controller/userController');
const authentication = require('./middleware/authentication');
//Product
router.post('/products',authentication,productController.create);
router.get('/products',authentication,productController.findAll);
router.get('/products/available',productController.available);// harus diatas yang pakeid id
router.get('/products/:id',productController.findById);
router.put('/products/:id',authentication,productController.update);

//User
router.get('/user',userController.Read);
router.post('/user/register',userController.register);
router.post('/user/login',userController.login);


module.exports = router