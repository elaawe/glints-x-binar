const router = require('express').Router();
const productController = require('./controller/productController');
const userController = require('./controller/userController');

//Product
router.post('/products',productController.create);
router.get('/products',productController.findAll);
router.get('/products/available',productController.available);// harus diatas yang pakeid id
router.get('/products/:id',productController.findById);

//User
router.get('/user',userController.Read);
router.get('/user/me',userController.findById);
router.post('/user/register',userController.register);
router.post('/user/login',userController.login);


module.exports = router