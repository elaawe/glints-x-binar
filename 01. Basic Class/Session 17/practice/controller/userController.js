const { user: User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

    /*
    bikit inpt data
    change plain password into encrypted pasword using bcrypt
    the save
    cek it
    
    */
    Read(req, res) {
        User.findAll() 
            .then(instance => { // karna findAll akan menghasilkan array maka autoname nya harus jamak
                res.status(200).json({
                    status:"success",
                    data: {
                        instance
                    }
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: 'errorr obtaining data',
                    errors: [err.message]
                })
            });
    },



    register(req, res) {
       // const salt = bcrypt.genSaltSync(10); // 10 means how much difficulties password will encrypted
        //const hash = bcrypt.hashSync(req.body.encrypted_password, salt);
        
        User.create({
           // email: req.body.email.toLowerCase(),
            email: req.body.email,
        //    encrypted_password: hash  //because encryption password or hash const already declared so we only put the hash variable        
            encrypted_password: req.body.encrypted_password
    })
        .then(user => {
            res.status(200).json({
                status: "success",
                data: {
                    user
                }
            })
        })
        .catch(err => {
            res.status(422).json({
                status: "fail",
                errors: [err.message]
            })
        })
    },

    login(req, res) {
    User.findOne({
        where: { email: req.body.email.toLowerCase() }
      })
        .then(instance => {
          if (!instance)
            return res.status(401).json({
              status: 'fail',
              errors: ["Email doesn't exist"]
            })
            console.log(instance.encrypted_password)
        
  
          let isPasswordValid = bcrypt.compareSync(req.body.encrypted_password, instance.encrypted_password);
  
          if (!isPasswordValid)
            return res.status(401).json({
              status: 'fail',
              errors: ["Wrong password!"]
            })
          let token = jwt.sign({ 
            id: instance.id,
             email: instance.email 
         }, 'rahasia');
          res.status(201).json({
            status: 'success',
            data: {
              user: token
            }
          })
        })
        .catch(err => {
          res.status(422).json({
            status: 'fail',
            errors: [err.message]
          })
        })
    },
    findById(req, res) {
        let token = req.headers.authorization;

        let payload;

        try { 
            payload = jwt.verify(token, 'rahasia')
        }
        catch(err) {
            return res.status(401).json({
                status: "fail",
                errors: [
                    "invalid Token"
                ]
            })
        }
        User.findByPk(payload.id)
        .then(instance => {
            res.status(201).json({
                status: "success",
                data: { instance
                }
            })
        })
        /*.catch(err => {
            res.status(422).json({
                status: 'errorr obtaining data',
                errors: [err.message]
            })
        })*/
    }
    /*
    login(req, res) {
        User.findOne({
            where: {email:req.body.email.toLowerCase()}
        })
        .then((user) => {
            if(bcrypt.compareSync(req.body.encrypted_password,user.encrypted_password)) { //validating password is it match with the stored datat or not
                res.status(202).json({
                    status: "success",
                    message: "login successfully"
                })
            } else {
                res.status(400).json({
                    status: "fail",
                    message: "Incorect Password"
                })
            }
        })
        .catch(err => {//if user input wrong email or there is no match email will throw this error message
            res.status(422).json({
                status: "fail login!",
                errors: [err.message]
            })
        })
    }*/
}
