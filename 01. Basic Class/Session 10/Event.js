const EventEmiter =  require('events'); // class 
const random =  new EventEmiter(); // instan, kaya Human: adrian, aryo(instan)

random.on('Register', ass => { // event listener menggunakan callback function
    console.log(`Sending email ${ass}`)
})

random.addListener('Register', () => { // bisa pake .on atau .addListener
    console.log("Function lain")
})
random.emit("Register", "hello word"); // sebuah event terjadi ketika di emit
// tidak boleh menaruh emit di sebelum event listener