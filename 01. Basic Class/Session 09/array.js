let participants = [
    "galih",
    "fikri",
    "adrian",
    "timotius",
    "rijal",
    "Titan",
    "imam",
    "nat",
    "Ian"
];

let person = participants;
console.log(participants);
person.push("adek");// nambahin data

//clone
let clone = [...participants];// ngeclone persis
clone.pop(); // paling akhir akan terhapus

console.log("ClonePopped:", clone);
console.log("Raw:", participants);

// Clone and Modify
let raw = [...persons] // Make raw
raw.splice(Math.floor(persons.length / 2), 1, "Hantu Tengah") // Adjust the array
// belajar
// .splice
// .slice
// .push
// .pop
// .sort
// .filter
// .forEach
// .map

let another = ["Hantu Pertama", ...raw, "Hantu Terakhir"];
console.log("CloneAndModify:", another);
