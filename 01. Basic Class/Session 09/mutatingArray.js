let participants = [
    "galih",
    "fikri",
    "adrian",
    "timotius",
    "rijal",
    "Titan",
    "imam",
    "nat",
    "Ian"
];

//push and pop
participants.push("hantu terakhir"); // nambah data di terakhir array
participants.pop(); // pop akan menghapus data terakhir dari array

// shift and unshift
participants.unshift("hantu pertama"); // nmabah data di paling depan array
participants.shift();// hapus data paling awal array

let midIndex = Math.floor(participants.length / 2);
participants.splice(midIndex,-1, "hantu tengah");
participants.splice(participants.indexOf("hantu tengah"), 1);