const obj = {
    name: 'fikri',
    address: 'solo'
}

const person = obj; //person nunjun obj jadi alat pemanggil

person.name = "another name" // coba di ganti nama tapi bukan obj.name caranya gitu
console.log(obj); // another person

//how to clone
const clone = {...obj};// ... ini adalah spread operator untuk ngeclone
clone.name = "rizky"; //ketika di clone dan rename tidak akan mengganti name yang lalu karna yang di ganti yang clone
console.log(clone);

const another = {...obj, name : "fikrie", isMarried: false}
console.log('another:', another);