var id = 0

function register(email, password, passwordConfirmation) {
    if (password !== passwordConfirmation) {
        throw new Error("Password doesn\'t match")
    }

    return {
        id: ++id,
        email,
        password
    }
}
const Fikri = register("test@mail.com", "12345", "12345");
console.log(fikri);

//pelajari unary a++ dan ++a