const person = {
    name: "Adrian",
    address: "jogja"
}

console.log(person.name) // keluar = adrian

/*
nah mau nambahin object didalam constan tadi yaitu
*/
person.isMarried = true; // using “ .
person.gender = "male"
//atau pake bracket
person["Pets"] = ['cats', 'birds'] // using [propsname]

person[person.gender == "male" ? "wife" : "husband"] = {
    name: "someone",
    gender: person.gender == "male" ? "male" : "female"

}
console.log(person);