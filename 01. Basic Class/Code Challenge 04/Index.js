const express = require('express');
const app = express();
const cors = require('cors');
const router = require('./router');
const morgan = require('morgan');
exception = require('./middlewares/exceptionHandler')


app.use(morgan('dev'));

app.use(express.json());

app.use(cors());

app.get('/',(req,res) => {
    res.status(200).json({
        status: "Success",
        message: "hello world"
    })
})

app.use(express.urlencoded({extended: true}));

app.use('/api/v1',router);

exception.forEach(handler =>
    app.use(handler)
  );

app.listen(8888, () => {
    console.log(`Server 8888 is running!`)
})