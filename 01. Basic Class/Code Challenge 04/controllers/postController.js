const { post } = require('../models');
const success = require('../middlewares/successHandler');

exports.create = async function(req, res, next) {
    try {
        const ngepost = await post.create({
            title: req.body.title,
            body: req.body.body,
            user_id: req.user.id
        });
        success(res, 201, ngepost, 'post');
    }
    catch(err) {
        res.status(422);
        next(err)
    }

}

exports.all =  async function(req, res, next) { 
   try{ 
       
    let data;

            if(req.user.role == 'admin') data = await Posts.findAll({
                include: 'thor'
            });
            if(req.user.role == 'member') data = await Posts.findAll({
                where:{
                    approved:true
                }
            })
            success(res, 201, data, "posts")
    
    success(res, 200, posts, 'posts');
    }
    catch(err) {
        res.status(422);
        next(err)
    }

}

exports.update = async function(req, res, next) {
    
   try {
       await post.update(req.body, {
           where: { id: req.params.id }
       })
       success(res, 200, "Succesfully updated", 'message')
   }
   catch(err) {
       res.status(422);
       next(err);
   }
}

exports.delete = async function( req, res, next) {

    try {
       await post.destroy(req.body, {
           where: { id: req.params.id }
       })
       success(res, 200, "Succesfully deleted", 'message')
   }
   catch(err) {
       res.status(422);
       next(err);
   }
}