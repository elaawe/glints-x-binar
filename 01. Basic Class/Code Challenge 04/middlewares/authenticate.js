const jwt = require('jsonwebtoken');
const { user : User } = require('../models');


module.exports = async (req, res, next) => {
         try {
            let token = req.headers.authenticate; 
            let payload = await jwt.verify(token, 'rahasia');
            
            let user = await User.findByPk(payload.id)
            req.user = user;
            next();
            
        }
        catch(err) {
            return res.status(401).json({
                status: "fail",
                errors: [
                    "invalid Token"
                ]
            })
        }
    }
      
