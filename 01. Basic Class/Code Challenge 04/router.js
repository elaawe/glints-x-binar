const router = require('express').Router();
const userController = require('./controllers/userController');
const authenticate = require('./middlewares/authenticate');
const postController = require('./controllers/postController');
const checkOwnership = require('./middlewares/checkOwnership');


router.post('/user/register',userController.register);
router.post('/user/login',userController.login);

router.post('/post',authenticate,postController.create);


module.exports = router