'use strict';
module.exports = (sequelize, DataTypes) => {
  const post = sequelize.define('post', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    user_id: DataTypes.INTEGER,
    approved: {
      type: DataTypes.BOOLEAN,
      defaultValue:false
    }
  }, {});
  post.associate = function(models) {
    // associations can be defined here
    post.belongsTo(models.user, {
      foreignKey: 'user_id',
      as: 'author'
    })
  };
  return post;
};