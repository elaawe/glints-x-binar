const Product = require('../models').product;
const Op = require('sequelize');
module.exports = {
    create(req,res) {
        Product.create(req.body)
            .then(product => {
                res.status(201).json({
                    status: "success",
                    data: {
                        product
                    }
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                })
            })
    },
    findAll(req, res) {
        Product.findAll()
        .then(products => { // karna findAll akan menghasilkan array maka autoname nya harus jamak
            res.status(200).json({
                status:"success",
                data: {
                    products
                }
            })
        })
        .catch(err => {
            res.status(422).json({
                status: 'errorr obtaining data',
                errors: [err.message]
            })
        })
    },
    findById(req, res) {
        Product.findByPk(req.params.id)
        .then(product => {
            res.status(201).json({
                status: "success",
                data: { product
                }
            })
        })
        .catch(err => {
            res.status(422).json({
                status: 'errorr obtaining data',
                errors: [err.message]
            })
        })
    },
    available(req, res) {
        Product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0 //GT greater than
                }
            }
        })
        .then(products => {
            res.status(422),json({
                status: 'fail',
                errors: [err.message]
            })
        })
    },
    update(req, res) {
        Product.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        .then
    }

}

