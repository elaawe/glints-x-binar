'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    email: { 
      type: DataTypes.STRING,
      validate: {
        isEmail: true,
        isLowerCase: true
      }
    },
    encrypted_password: { 
      type:DataTypes.STRING,
      validate: {
        min: 6
      }
    }
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },

      beforeCreate: instance => {
        console.log("ini beforeCreate Hooks");
        instance.encrypted_password = bcrypt
          .hashSync(instance.encrypted_password, 10)
      }
    }
  });
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};