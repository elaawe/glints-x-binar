module.exports =(res, code, instance) => {
    let result = res.status(code).json({
                 status: "Success",
                 instance
                })
    return result
};
