const express = require('express');
const app = express();
const router = require('./router');
const morgan = require('morgan');

app.use(morgan('dev'));// pake tiny lebis simple dari combined, dev juga simple dan ada warna
app.use(express.json());


app.get('/',(req,res) => {
    res.status(200).json({
        status: "Success",
        message: "hello world"
    })
})

//use the router middleware
app.use('/',router);
app.use(express.urlencoded({extended: true}));

exception.forEach(handler =>
    app.use(handler)
  );
  

app.listen(3000,() => {
    console.log("Listening on port 3000")
})