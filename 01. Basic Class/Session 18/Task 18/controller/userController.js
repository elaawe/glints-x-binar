const { user: User } = require('../models');
const bcrypt = require('bcryptjs');
const success = require('../middlewares/successHandler')

module.exports = {
      register(req, res) {
        User.create({
            email: req.body.email,     
            encrypted_password: req.body.encrypted_password
    })
        .then(instance => success(res, 200, instance))
        .catch(err => {
            res.status(422);
            next(err)
        })
    },
      Read(req, res) {
        User.findAll() 
            .then(instance => success(res, 201, instance))
            .catch(err => {
                res.status(422);
                next(err)
            });
    },
    login(req, res) {
    User.findOne({
        where: { email: req.body.email.toLowerCase() }
      })
        .then(instance => {
          if (!instance)
            (err => {
              res.status(401);
              next(`${req.body.email} doesn'\t exist`)

            })
  
          let isPasswordValid = bcrypt.compareSync(req.body.encrypted_password, instance.encrypted_password);
  
          if (!isPasswordValid)
            (err => {
              res.status(401);
              next(`Wrong Password`)

            })
  
          let token = jwt.sign({ 
            id: instance.id,
             email: instance.email 
         }, 'rahasia');
          res.status(201).json({
            status: 'success',
            data: {
              user: token
            }
          })
        })
        .catch(err => {
          res.status(422);
          next(err);
          console.log(next(err));
        })
    },
    me(req, res) {
        res.status(200).json({
            status: 'success',
            data: {
                user: req.user
            }
        })
    }
   
   
}
