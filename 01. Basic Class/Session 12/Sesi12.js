
//Modul class record for another class
// product name stock
const fs = require('fs');

class Record {
  constructor(props) {
    if (this.constructor == Record) 
      throw new Error("Can't instantiate from Record");
    
    this._validate(props);
    this._set(props);
  }

  _validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
      throw new Error("Props must be an object");
    

    this.constructor.properties.forEach(i => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${this.constructor.name}: ${i} is required`)
    })
  }

  _set(props) {
    this.constructor.properties.forEach(i => {
      this[i] = props[i];
    })
  }

  get all() {
    try {
      return eval(
        fs.readFileSync(`${__dirname}/${this.constructor.name}.json`)
          .toString()
      )
    }
    catch {
      return []
    }
  }

  find(id) {

  }

  update(id) {

  }

  delete(id) {

  }

  save() {
    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify([...this.all, { id: this.all.length + 1, ...this } ], null, 2)
    );
  }
}

/*
  Make two class who inherit Abstract Class called Record 

  Book,
    title
    author
    price
    publisher

  Product,
    name,
    price,
    stock
*/

class Book extends Record {

    static properties = [
        "title",
        "author",
        "price",
        "publisher"
    ]

    save() {
        
        this.all.forEach(i => {
            if(i.title == this.title)
            throw new Error("Book already taken");
        })
        super.save();
    }
  

  /*  constructor(props) {
        super(props);
        this.title = props.title;
        this.author = props.author;
        this.price = props.price;
        this.publiser = props.publiser;

    } */
}


class Product extends Record {

  static properties = [
      "name",
      "price",
      "stock",
  ]
}

let bookOne = new Book({
    title: "Harry Potter" ,
    author: "J. K. Rowling",
    price: "Rp. 1000",
    publisher: "Malioboro Publisher"
})

let bookTwo = new Book({
    title: "Master Chef" ,
    author: "Arnold Poernomo",
    price: "Rp. 2000",
    publisher: "Gramedia"
})

let Product1 = new Product({
  name: "mainan",
  price: "Rp 500",
  stock: 300
})


bookOne.save();
bookTwo.save();
Product1.save()