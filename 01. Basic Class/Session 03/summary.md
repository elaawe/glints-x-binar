adrian/22 April 2020

# What is GIT? #

Git is a Distributed verison control system, that means git is a system that provide programer to manage a code,create a code, edit a code, collaborate with the other develop freely and trackable.

git has some service provider like GitLab, Github, BucketHub

## What is Gitlab? ##
Gitlab is one of Git service Provider that has a function just like a Google Drive for a programer

First You have to make Your own account on Gitlab.com

Then make your own project

## Connecting Git Repository with our Local repository 
1. make your own  folder on local repository/your computer
2. then initialize your folder with
```javascript
:git init yourfoldername
```
3. register your computer email and user name to a git
```javascript
:git config --global user.email "email@example.com"
:git config --global user.name "yourusername"
```
4. Cek your git status
```javascript
:git status
```
5. how to make a file
    1. make your own file(whatever it is) on terminal or directly on your text editor
    2. write your content and then save
    3. add to a git
    ```javascript
    :git add yourfilename
    ```
    4. commit your file and give some remarks(it useful in the future to track the history) 
    ```javascript
    :git commit -m "yourremarks"
    ```
    5. how to discard change in our file(just in case before we add/commit the file)
    ```javascript
    :git checkout --yourfilename
    ```
    6. how to reset change in our file(just in case we already add and befor we commit it)
    ```javascript
    :git reset yourfilename
    ```
6. what if we stuck in the tereminal text editor?
```
ctr + X then Shift + Y then Enter
```
7. how to track your git change history?
```javascript
:git log
and press Q to quit the log history
```
8. how to connect our local repository folder project to a git repository(if we already had thefolder and want to connect it to gitlab repository)
    1. Open Gitlab.com
    2. new project
    3. Scroll down and copy the "push an existing Git epository section" command
    ```javascript
    :git remote add yourremotename git@gitlab.com:youraccountname/yourprojectname.git

    :git push -u yourremotename yourbranchname
    ```
9. how to check your all remote(just in case you have many remote on gitlab, github, etc)
```javascript
:git remote -v
```
10. how to check where or what branch we already have
```javascript
:git branch
```
11. how to make a new branch
```javascript
:git branch <yournewbranchname>
```
or
```javascript
git checkout -b <yournewbranchname> //to make a new branch also move to this new branch
```
12. how to move from one branch to another
```javascript
:git checkout <branchname>
```
13. if the new branch created we should push it to gitlab to syncronize
```javascript
:git push remotename <newbranchname>
```
14. how to merge a branch
    1. get in to your branch(merge to be branch)
    2. scroll down and choose "merge request" from the leftside menus.
    3. and then make sure where is the  branch destination to merge is
    4. next and choose whether you want to delete your previous branch or not and the click merge button
15. if we already have a project in gitlab.com and we want to download it to our local repository and make it as our git local repository so we can do editing from local reposirtory(CLONE)
    1. get in to your branch you want to clone
    2. choose clone menus on the right above
    3. copy the link SSH or HTTPS
    4. paste to your terminal after the command 
    ```javascript
    :git clone <paste your ssh key link here>
    ```
