const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
// we should clean the data from null before sorting using filter that already prepared
  data = clean(data);
  //looping from index 0 
  for(let i = 0 ; i < data.length ; i++) {
    ///looping from index 0+1
    for(let j = i + 1; j < data.length; j++) {
      //the conditional function to check whether the data[i] is greater than data[j]
        if(data[i] > data[j]) {
            let A = data[i];
            data[i] = data[j];
            data[j] = A;
            }
        }
    }   
    return data;   
}
console.log(sortAscending(data));

// Should return array
function sortDecending(data) {
  data = clean(data);
  for(let i = 0 ; i < data.length ; i++) {
    for(let j = i + 1; j < data.length; j++) {
      //the conditional function to check whether the data[i] is not greater than data[j]
        if(data[i] < data[j]) {
            let A = data[i];
            data[i] = data[j];
            data[j] = A;
            }
        }
    }   
  return data;
}
console.log(sortDecending(data));

// DON'T CHANGE
test(sortAscending, sortDecending, data);
