const express = require('express');
const router = express.Router();
const product = require('./controller/controller');

/* Product API Collection */
router.post('/products/input', product.create);
router.get('/products', product.read);
router.get('/products/:id', product.read);
router.put('/products/:id', product.update);
router.delete('/products/:id', product.deleteById);

/* User API Collection */

module.exports = router;
