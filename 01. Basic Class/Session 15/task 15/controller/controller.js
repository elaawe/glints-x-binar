//const Product = require('../db/product.json');
const Product = require('../models').product;


function create(req, res) {
  const newProduct = {
    name: req.body.name,
    price: req.body.price,
    stock: req.body.stock
  }
  Product.create(newProduct)
  .then(data => res.status(200).json({
    status: true,
    product: data
  }))
  .catch(err => {        
    res.send(err.message);  
  })
}

function read(req, res) {
  Product.findAll().then(data =>
    res.status(200).json({
      status: true,
      data
    })
  )
}

function readById (req, res) {
  Product.findOne({
      where: {id: req.params.id}
  })
  .then(data => res.status(200).json({
      status: true,
      data
    })
  )
  .catch(err => {
    res.send(err.message)
  })
}


function update(req, res) {
  Product.update({
    name: req.body.name,
    price: req.body.price,
    stock: req.body.stock
  }, {
    where: {
      id: req.params.id
    }
  })
    .then(() => {
      res.send(`Data with id ${req.params.id} success to updated`);
    })
    .catch(err => {
      res.send(err.message)
  })
}

function deleteById(req, res) {
  Product.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(() => {
      res.send(`Data with id ${req.params.id} success deleted`);
    })
    .catch(err => {
      res.send(err.message);
    });
}

module.exports = {
    create, read, readById, update, deleteById
  }
    