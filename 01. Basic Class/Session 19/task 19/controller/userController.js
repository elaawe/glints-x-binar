const { user: User } = require('../models');
const bcrypt = require('bcryptjs');
const success = require('../middlewares/successHandler');
const jwt = require('jsonwebtoken')

module.exports = {
      register(req, res, next) {
        User.create({
            email: req.body.email,     
            encrypted_password: req.body.encrypted_password
    })
        .then(instance => success(res, 200, instance, 'user'))
        .catch(err => {
            res.status(422);
            next(err)
        })
    },
      Read(req, res, next) {
        User.findAll() 
            .then(instance => success(res, 201, instance, 'user'))
            .catch(err => {
                res.status(422);
                next(err)
            });
    },
    login(req, res, next) {
      User.findOne({
        where: { email: req.body.email.toLowerCase() }
      })
      .then(instance => {
        if (!instance) {
          (err => {
            res.status(401);
            next(' email doesnt exist')})
        }
        else {
            let isPasswordValid = bcrypt.compareSync(req.body.encrypted_password, instance.encrypted_password);
  
            if (!isPasswordValid) {
                  (err => {
                    res.status(401);
                    next(`Wrong Password`)
                  })
            }
            let token = jwt.sign({ 
              id: instance.id,
              email: instance.email }, 'rahasia');
              success(res, 201, token, 'token')
            }
        }
    })
    .catch(err => {
          res.status(422);
          next(err);
        })
    },
    me(req, res, next) {
        success(res, 201, req.user, 'user')
    },

    async createProfile(req, res, next) {
      try {
        let profile = await Profile.create({
          bio: req.body.bio,
          phone_number: req.body.phone_number,
          address: req.body.address,
          user_id: req.user.id
        })
      }
      catch(err) {
        res.status(422);
        next(err);
      }
    }
   
   
}
