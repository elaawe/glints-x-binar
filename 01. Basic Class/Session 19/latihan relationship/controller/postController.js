const { Post } = require('../models');
const success = require('../middlewares/successHandler');

exports.create =async function(req, res, next) {
    try {
        const post = await Post.create({
            title: req.body.title,
            body: req.body.body,
            user_id: req.user.id
        });
        success(res, 201, post, 'post');
    }
    catch(err) {
        res.status(422);
        next(err)
    }

}

exports.all =  async function(req, res, next) { 
   try{ 
       
       const posts = await Post.findAll({
        include: 'thor'

    });
    let anuu = []
    const anu = posts.map(i => i.thor.encrypted_password);
    anuu.push(anu);
    success(res, 200, posts, 'posts');
    }
    catch(err) {
        res.status(422);
        next(err)
    }
}

exports.update = async function(req, res, next) {
    /*
    1. Check user id,
    2. then find the post
    3. if the posts.user_id is equal to req.user.id then update the post
    4. otherwise, reject.
    */
   try {
       await Post.update(req.body, {
           where: { id: req.params.id }
       })
       success(res, 200, "Succesfully updated", 'message')
   }
   catch(err) {
       res.status(422);
       next(err);
   }
}

exports.delete = async function( req, res, next) {
    
}