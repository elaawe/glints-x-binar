const express = require('express')
const app = express()
const user = require('./db/user.json');
const product = require('./db/products.json');

// Middleware to Parse JSON
app.use(express.json());

 // end poin di express
app.get('/', function (req, res) {
  res.send('Hello World')
})

app.get('/produk', function(req, res){
  console.log("this is end point products");
  res.send("ini products")
})
// GET /products
app.get('/products', function(req, res) {
  res.json(product);
})

app.get('/products/available', function(req, res) {
  res.json(product.filter(i => i.stock > 0))
})

app.get('/user', (req, res) => {
  let entity = {...user};
  delete entity.password
  res.json(entity)
}) 

/*
app.post('/logins', (req, res) => {
  console.log(req.body);
  res.end();
})
*/

app.post('/login', (req, res) => {
  if (user.email !== req.body.email)
    return res.status(401).json({
      status: false,
      message: "Email doesn't exist!"
    })

  if (user.password !== req.body.password)
    return res.status(401).json({
      status: false,
      message: "Wrong password!"
    })

  res.status(200).json({
    status: true,
    message: "Succesfully logged in"
  })
  
})

app.listen(3000, () => {
  console.log("Listening on port 3000!")
})