const express = require('express');
const router = express.Router();// to activated the router function from express
const resultOf = require('./controller/Controller');


router.get('/', function (req, res) {
    res.send('TASK 14')
})

router.get('/info', (req, res) => {
    res.status(200).json({
        status: true,
        data: [
            "POST /calculate/circleArea", // link
            "POST /calculate/tubeVolume",
            "POST /calculate/squareArea",
            "POST /calculate/cubeVolume",
            "POST /calculate/triangleArea",
        ]
    })
})

//to link the controler function with the index
router.post('/calculate/circleArea',resultOf.circleArea);
router.post('/calculate/tubeVolume',resultOf.tubeVolume);
router.post('/calculate/squareArea',resultOf.squareArea);
router.post('/calculate/cubeVolume',resultOf.cubeVolume);
router.post('/calculate/triangleArea',resultOf.triangleArea);


module.exports = router;