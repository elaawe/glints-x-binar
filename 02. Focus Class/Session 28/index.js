
require('dotenv').config();
const express = require('express')
const morgan = require('morgan')
const app = express()
const sgMail = require('@sendgrid/mail');

const { PORT = 8000 } = process.env

const router = require('./router')



app.use(morgan('dev'))
app.use(express.json())
app.use('/api/v1',router);

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
})


