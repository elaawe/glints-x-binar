const User = require('../models').user;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const sgMail = require('@sendgrid/mail');
require('dotenv')

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

async function register(req, res) {
    let {
        email
    } = req.body
    let  hash = await bcrypt.hashSync(req.body.password, 10)
    try {
        let instance = await User.create({
            email,
            password: hash
        })
      
        const msg = {
            to: instance.email,
            from: 'God@heaven.com',
            subject: 'Sending with Twilio SendGrid is Fun',
            text: 'and easy to do anywhere, even with Node.js',
            html: '<strong>and easy to do anywhere, even with Node.js</strong>',
        };
        sgMail.send(msg)
        
        const haha = await jwt.sign({ 
            id: instance.id,
            email: instance.email
        }, process.env.SECRET_KEY);
        
        res.status(200).json({
            status: 'Success',
            data: {
                User: instance,
                token: haha,
                verify: msg
            }
        })
    } catch (err) {
        res.status(422).json({
            status: 'Failed',
            message: err.message
        })
    }
}

module.exports = {
    register,
    // login
};