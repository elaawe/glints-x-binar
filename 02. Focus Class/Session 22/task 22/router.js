const router = require('express').Router();


//import controler
const post = require('./controllers/postController');
const index = require('./controllers/index');

router.get('/', index.home);

router.post('/post', post.create);
router.get('/post', post.index);
router.put('/post/:id', post.update);
router.delete('/post/:id', post.delete);


module.exports = router;