const request = require('supertest');
const app = require('../server');
const {
    Post
} = require('../models');

describe('Posts API Collection', () => {
    beforeAll(() => {
        return Post.destroy({
            truncate: true
        })
    })

    afterAll(() => {
        return Post.destroy({
            truncate: true
        })
    })

    describe('POST /post', () => {

            test('Should successfully create new post', done => {
                    request(app)
                        .post('/post')
                        .set('Content-Type', 'application/json')
                        .send({
                            title: 'Hello World',
                            body: 'lorem ipsum'
                        })
                        .then(res => {
                            expect(res.statusCode).toEqual(201);
                            expect(res.body.status).toEqual('success');
                            done();
                        })
                }),


                test('Create Failed test', done => {
                    request(app)
                        .post('/post')
                        .set('Content-Type', 'application/json')
                        .send({
                            title: '',
                            body: ''
                        })
                        .then(res => {
                            expect(res.statusCode).toEqual(422);
                            expect(res.body.status).toEqual('fail');
                            done();
                        })
                })
        }),

        describe('GET /post', () => {

            test('Should successfully get all the post', done => {
                request(app)
                    .get('/post')
                    .set('Content-Type', 'application/json')
                    .then(res => {
                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('success');
                        done();
                    })
            })
        }),
        describe('PUT /post/:id', () => {

            test('should succesfully update', done => {
                    request(app)
                        .put('/post/1')
                        .set('Content-Type', 'application/json')
                        .send({
                            title: 'Hello Hell',
                            body: 'loh he'
                        })
                        .then(res => {
                            expect(res.statusCode).toEqual(202);
                            expect(res.body.status).toEqual('success');
                            done();
                        })
                }),

                test('Update Failed test', done => {
                    request(app)
                        .put('/post/1')
                        .set('Content-Type', 'application/json')
                        .send({
                            title: null,
                            body: null
                        })
                        .then(res => {
                            expect(res.statusCode).toEqual(422);
                            expect(res.body.status).toEqual('fail');
                            done();
                        })
                })
        }),
        describe('DELETE /post/:id', () => {

            test('should succesfully delete', done => {
                request(app)
                    .delete('/post/1')
                    .set('Content-Type', 'application/json')
                    .then(res => {
                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('success');
                        done();
                    })
            })
            

        })
})