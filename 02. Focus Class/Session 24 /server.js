// Keep the import statement above
require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const router = require('./router');;

// Keep the initialization after the import statement
const app = express();

// Basic Express Configuration
app.use(express.json());
if (process.env.NODE_ENV !== 'test')
  app.use(morgan('dev'))
app.use('/', router);

/* Export the app,
 * because we need the
 * server instance to run somewhere else
 * */
module.exports = app;
