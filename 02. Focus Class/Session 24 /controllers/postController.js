const { Post } = require('../models');

module.exports = {
  index(req, res) {
    Post.findAll()
      .then(posts => {
        res.status(200).json({
          status: 'success',
          data: {
            posts
          }
        })
      }) 
  },

  create(req, res) {
    Post.create(req.body)
      .then(post => {
        res.status(201).json({
          status: 'success',
          data: {
            post
          }
        }) 
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        }); 
      })
  },

  delete(req, res) {
    Post.destroy({
      where: { id: req.params.id }
    })
      .then(data => {
        res.status(204).json({
          status: 'success',
          data: {
            message: data
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        })
      })
  },
}
