require('dotenv').config();
const express = require('express');
const app = express();
const morgan = require('morgan');
const PORT = process.env.PORT || 8888;
const router = require('./router');

app.set('view engine', 'pug')
app.use(express.json())

app.use(morgan('dev'))
app.use(router);


app.listen(PORT, () => {
    console.log(`Server started at ${Date()}`);
    console.log(`Listening on http://localhost:${PORT}`);
})