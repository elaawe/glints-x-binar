const express = require('express')
const router = express.Router();
const postContoller = require('./controllers/api/postController');
const indexController = require('./controllers/indexController');
const admin = require('./controllers/admin/')

router.get('/',indexController.home);
router.get('./admin/posts', admin.post.index)

router.post('/post',postContoller.create);

module.exports = router