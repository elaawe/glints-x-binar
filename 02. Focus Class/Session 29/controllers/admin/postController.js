const { Post } = require('../../models');

module.exports = {

    async index(req, res) {
        const post = await Post.findAll()
        res.render('post/index')
    }
}