'use strict';
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    title: {
      type:DataTypes.STRING,
      allowNull: false
    },
    body: {
      type:DataTypes.TEXT,
    },
    approved: DataTypes.BOOLEAN
    
  }, {});
  Post.associate = function(models) {
    // associations can be defined here
  };
  return Post;
};