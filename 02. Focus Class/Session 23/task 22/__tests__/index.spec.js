const request = require('supertest');
const app = require('../server');

//jest
describe('Root Path', () => {
    describe('GET /', () => {
        test('Should return 200', done => {        
            //supertest
            request(app).get('/')
                .then(res => {
                    //jest 
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    expect(res.body.message).toEqual('hello world');
                    // console.log(res.body);
                    //Done is importan to tell the function is already done 
                    done();
                })
        })
    })
})