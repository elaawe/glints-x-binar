const Post = require('../models/post')

module.exports = {
    all(req, res) {
        Post.find()
        .then(posts => {
            res.status(200).json({
                status: 'success',
                data: {
                    posts
                }
            })
        })
    },
    create(req, res) {
        const {title, body} = req.body;
        Post.create({title, body})
        .then(post => {
            res.status(201).json({
                status: 'success',
                data: {
                    post
                }
            })
        })
        .catch(err => {
            res.status(422).json({
                status: 'fail',
                errors: [err.message]
            })
        })
    }
}