const router = require('express').Router();

// Controllers
const index = require('./controllers/indexController')
const post = require('./controllers/postController')


router.get('/', index.home)
router.get('/posts',post.all)
router.post('/posts',post.create)

module.exports = router;
