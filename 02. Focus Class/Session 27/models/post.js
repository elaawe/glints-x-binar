const mongoose = require('mongoose')
const schema = new mongoose.Schema({
    title: {
        type:'string',
        require: true
    },
    body: {
        type: 'string',
        require: true
    },
    approved: {
        type: 'boolean',
        require: true,
        default: false
    }
})
const Post = mongoose.model('Post',schema)
module.exports = Post