const mongoose = require('mongoose')
const schema = new mongoose.Schema({
    user: {
        type:'string',
        require: true
    },
    password: {
        type: 'string',
        require: true
    }
})
const Post = mongoose.model('User',schema)
module.exports = User