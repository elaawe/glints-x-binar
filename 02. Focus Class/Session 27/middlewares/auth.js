module.exports = async function auth(req, res, next){

try {
    request.body.password = Bcrypt.hashSync(request.body.password, 10);
    var user = new UserModel(request.body);
    var result = await user.save();
    response.send(result);
} catch (error) {
    response.status(500).send(error);
}
}