// getting-started.js
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/learndb', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});