const express = require('express')
const morgan = require('morgan')
const app = express()
const session = require('express-session')
const flash = require('express-flash')

require('dotenv').config()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(morgan('dev'))
app.use(flash())
app.use(
  session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: false
  })
)
app.set('view engine', 'pug')

const router = require('./router')
app.use(router)

module.exports = app
